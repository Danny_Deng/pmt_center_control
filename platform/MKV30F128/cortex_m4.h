/* Cortex-M4 System timer */
#define SYST_CSR            (*(volatile uint32_t *)0xE000E010)
#define SYST_RVR            (*(volatile uint32_t *)0xE000E014)
#define SYST_CVR            (*(volatile uint32_t *)0xE000E018)
#define SYST_CALIB          (*(volatile uint32_t *)0xE000E01C)

/* Cortex-M4 NVIC */
#define NVIC_ISER                ( (volatile uint32_t *)0xE000E100)
#define NVIC_ICER                ( (volatile uint32_t *)0xE000E180)
#define NVIC_ISPR                ( (volatile uint32_t *)0xE000E200)
#define NVIC_ICPR                ( (volatile uint32_t *)0xE000E280)
#define NVIC_IABR                ( (volatile uint32_t *)0xE000E300)
#define NVIC_IPR                 ( (volatile uint8_t *)0xE000E400)
#define NVIC_STIR                (*(volatile uint32_t *)0xE000EF00)

/* Cortex-M4 SCB */
#define SCB_ACTLR               (*(volatile uint32_t *)0xE000E008)
#define SCB_CPUID               (*(volatile uint32_t *)0xE000ED00)
#define SCB_ICSR                (*(volatile uint32_t *)0xE000ED04)
#define SCB_VTOR                (*(volatile uint32_t *)0xE000ED08)
#define SCB_AIRCR               (*(volatile uint32_t *)0xE000ED0C)
#define SCB_SCR                 (*(volatile uint32_t *)0xE000ED10)
#define SCB_CCR                 (*(volatile uint32_t *)0xE000ED14)
#define SCB_SHPR                ( (volatile uint8_t *)0xE000ED14)
#define SCB_CFSR                (*(volatile uint32_t *)0xE000ED28)
#define SCB_MMSR                (*(volatile uint32_t *)0xE000ED28)
#define SCB_BFSR                (*(volatile uint32_t *)0xE000ED29)
#define SCB_UFSR                (*(volatile uint32_t *)0xE000ED2A)
#define SCB_HFSR                (*(volatile uint32_t *)0xE000ED2C)
#define SCB_MMFAR               (*(volatile uint32_t *)0xE000ED34)
#define SCB_BFAR                (*(volatile uint32_t *)0xE000ED38)



/*--------------------------------------------------------------*/
/* Cortex-M4 core/peripheral access macros                      */
/*--------------------------------------------------------------*/

/* These are for only privileged mode */
#define __enable_irq() __asm volatile("CPSIE i\n")
#define __disable_irq() __asm volatile("CPSID i\n")
#define __enable_irqn(n) NVIC_ISER[(n) >> 5] = 1 << ((n) & 0x1F)
#define __disable_irqn(n) NVIC_ICER[(n) / 32] = 1 << ((n) % 32)
#define __test_irqn_enabled(n) (NVIC_ISER[(n) / 32] & (1 << ((n) % 32)))
#define __set_irqn(n) NVIC_ISPR[(n) / 32] = 1 << ((n) % 32)
#define __clear_irqn(n) NVIC_ICPR[(n) / 32] = 1 << ((n) % 32)
#define __test_irqn(n) (NVIC_ICPR[(n) / 32] & (1 << ((n) % 32)))
#define __test_irqn_active(n) (NVIC_IABR[n / 32] & (1 << ((n) % 32)))
#define __set_irqn_priority(n, v) NVIC_IPR[n] = (v)
#define __set_faultn_priority(n, v) SCB_SHPR[(n) + 16] = (v)
#define __get_MSP()                                                            \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, MSP\n" : "=r"(__rv));                                         \
    __rv;                                                                      \
  })
#define __get_PSP()                                                            \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, PSP\n" : "=r"(__rv));                                         \
    __rv;                                                                      \
  })
#define __get_PRIMASK()                                                        \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, PRIMASK\n" : "=r"(__rv));                                     \
    __rv;                                                                      \
  })
#define __get_FAULTMASK()                                                      \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, FAULTMASK\n" : "=r"(__rv));                                   \
    __rv;                                                                      \
  })
#define __get_BASEPRI()                                                        \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, BASEPRI\n" : "=r"(__rv));                                     \
    __rv;                                                                      \
  })
#define __get_CONTROL()                                                        \
  ({                                                                           \
    uint32_t __rv;                                                             \
    __asm("MRS %0, CONTROL\n" : "=r"(__rv));                                     \
    __rv;                                                                      \
  })
#define __set_MSP(arg)                                                         \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR MSP, %0\n" ::"r"(__v));                                           \
  }
#define __set_PSP(arg)                                                         \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR PSP, %0\n" ::"r"(__v));                                           \
  }
#define __set_PRIMASK(arg)                                                     \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR PRIMASK, %0\n" ::"r"(__v));                                       \
  }
#define __set_FAULTMASK(arg)                                                   \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR FAULTMASK, %0\n" ::"r"(__v));                                     \
  }
#define __set_BASEPRI(arg)                                                     \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR BASEPRI, %0\n" ::"r"(__v));                                       \
  }
#define __set_CONTORL(arg)                                                     \
  {                                                                            \
    uint32_t __v = arg;                                                        \
    __asm("MSR CONTROL, %0\nISB\n" ::"r"(__v));                                  \
  }
