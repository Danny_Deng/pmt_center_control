#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "uart.h"
#include "system.h"
#include "LPC1769.h"
#include "rs485.h"
#include "regdef.h"
#include "fifo.h"

/* PC transmit macro */
#define PC_trm(format, args...) printf("\r"format, ##args)

extern volatile char uart_buf[16];
extern volatile uint8_t cnt;
extern volatile uint8_t fgg;

fifo_t fifo;

int main (void) {
    setbuf(stdout, NULL);
    setbuf(stdin, NULL);

    system_clock_init();
    system_power_init();
    pinconnect_init();
    uart3_init();

    init_hw_rs485();

    uint8_t buf[256];
    init_fifo(&fifo, buf, sizeof(buf));
    // volatile uint8_t *ptr = &fifo.fg;
    PC_trm("Start main code\n");
    // __enable_irqn(UART0_IRQn);
    uint16_t c = 0;
    while(true) {
        // if(*ptr == 1) {
        //     uint8_t temp = dequeue(&fifo);
        //     uart3_putc(temp);
        //     PC_trm("Hello World !!!\n");
        // }
        // ENABLE_RS485_TRM();
        // uart0_putc(0xaa);
        // DISABLE_RS485_TRM();

        for(uint32_t i=0;i<100000;i++) __asm("nop");
        PC_trm("Hello World !!!, %d\n", c++);
    }

    return 0;
}
