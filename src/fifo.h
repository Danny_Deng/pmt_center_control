#ifndef FIFO_H
#define FIFO_H

#include <stdint.h>

#define SM_READY         0
#define SM_ENQUEUE_FULL  1

typedef struct _fifo {
    volatile uint16_t index_in;
    volatile uint16_t index_out;
    volatile uint16_t max_num;
    volatile uint8_t *data;
    volatile uint8_t sm;
    volatile uint8_t fg;
}fifo_t;

void init_fifo(fifo_t *fifo, uint8_t *data, uint16_t data_size);
void enqueue(fifo_t *fifo, uint8_t data);
uint8_t dequeue(fifo_t *fifo);

#endif
