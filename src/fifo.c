#include "fifo.h"
#include <stdint.h>


void init_fifo(fifo_t *fifo, uint8_t *data, uint16_t data_size) {
    fifo->data = data;
    fifo->index_in = 0;
    fifo->index_out = 0;
    fifo->sm = SM_READY;
    fifo->fg = 0;
    fifo->max_num = data_size;
}

void enqueue(fifo_t *fifo, uint8_t data) {
    fifo->data[fifo->index_in] = data;
    fifo->index_in++;
    fifo->fg = 1;
    if(fifo->index_in == fifo->max_num) fifo->index_in = 0;

    // if((fifo->index_out > fifo->index_in) && (fifo->sm == SM_READY)) {
    //     // error
    // }
    // else {
    //     fifo->data[fifo->index_in] = data;
    //     fifo->index_in++;
    //     fifo->fg = 1;
    //     if((fifo->index_in == fifo->max_num) && (fifo->sm == SM_ENQUEUE_FULL)) {
    //         // error
    //     }
    //     else if(fifo->index_in == fifo->max_num) {
    //         fifo->index_in = 0;
    //         fifo->sm = SM_ENQUEUE_FULL;
    //     }
    // }

}

uint8_t dequeue(fifo_t *fifo) {
    if((fifo->index_out > fifo->index_in) && (fifo->sm == SM_READY)) {
        // error
    }
    else {
        uint8_t temp = fifo->data[fifo->index_out];
        fifo->data[fifo->index_out] = 0;
        if(fifo->index_in == fifo->index_out) {
            fifo->fg = 0;
        }
        fifo->index_out++;
        if(fifo->index_out == fifo->max_num) {
            fifo->index_out = 0;
            fifo->sm = SM_READY;
        }
        return temp;
    }
    return 0;
}

