#include "uart.h"
#include "LPC1769.h"
#include <string.h>
#include <stdio.h>
#include "fifo.h"

extern volatile char send_buf[16];

volatile char uart_buf[16];
volatile uint8_t cnt = 0;
volatile uint8_t fgg = 0;
volatile uint32_t st = 0;
extern fifo_t fifo;

void UART0_IRQHandler(void) {
    enqueue(&fifo, U0RBR);
    // uart3_putc(U0RBR);
}
