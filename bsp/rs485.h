#ifndef RS485_H
#define RS485_H
#include <stdint.h>

#define ENABLE_RS485_TRM()  FIO0SET1 |= (1<<1)
#define DISABLE_RS485_TRM() FIO0CLR1 |= (1<<1)

void init_hw_rs485(void);
void uart0_init(void);
void uart0_putc(uint8_t data);
uint8_t uart0_getc(void);

#endif /* RS485_H */
