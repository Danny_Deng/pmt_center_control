#include <stdint.h>
#include "rs485.h"
#include "LPC1769.h"
#include "regdef.h"

/**
 * @brief ASA BUS RS485 IO
 * ASA_TX:  P0[2]
 * ASA_RX:  P0[3]
 * ASA_485: P0[9]
 *
 * Master mode, MS pin is High
 */

void init_hw_rs485(void) {
    // set RS485 as TXD0 and RXD0
    // set control pin
    PCONP |= PCONP_UART0 | PCONP_GPIO;
    FIO0DIR1 |= (1<<1);
    FIO0CLR1 |= (1<<1);
    DISABLE_RS485_TRM();

    // set RS485 TXD0 RXD0
    uart0_init();
    PINSEL0 |= (PIN_FUNC_1 << 4) | (PIN_FUNC_1 << 6);
}

#define U0_MUL_VAL 14
#define U0_DIVADD_VAL 5

#define U0_DLL_VAL 12
#define U0_DLM_VAL 0

/**
 * pclk_uart3 = cpu_clk / 4
 *            = 120e6   / 4
 *            = 30e6
 *
 * baudrate = pclk / 16 / (256*DLM + DLL) / (1+ DIV/MUL)
 *          = 30e6 / 16 / (256* 0  +  12) / (1+  5 /  14)
 *          = 115131.5789
 *
 * err = (115131.5789-115200)/115200*100
 *     = 0.059%
 */
void uart0_init(void) {

    // enable uart fifo
    U0FCR = USART_FCR_FIFOEN;

    // get acess for Divisor Latch
    U0LCR = USART_LCR_DLAB;

    // set baudrate
    U0FDR = (U0_MUL_VAL << USART_FDR_MULVAL_POS) |
                     (U0_DIVADD_VAL << USART_FDR_DIVADDVAL_POS);
    U0DLL = U0_DLL_VAL;
    U0DLM = U0_DLM_VAL;

    // set character length as 8-bit
    // clr access for Divisor Latch
    U0LCR = (3 << USART_LCR_WLS_POS);

    U0IER |= 1<<2;
}

void uart0_putc(uint8_t data) {
    U0THR = data;
    // In datasheet TEMT is set when both UnTHR and UnTSR are empty
    // TEMT is cleared when either the UnTSR or the UnTHR contain valid data.
    while(!(U0LSR & USART_LSR_TEMT));
}

uint8_t uart0_getc(void) {
    while (!(U0LSR & USART_LSR_RDR)) {
        ;
    }
    return U0RBR;
}

